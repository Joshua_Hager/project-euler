class PE17
	def initialize
		@currentInt = 1 #loop counter
		@counter = 0 #number of letters in the words
		@onesArray = Array["one","two","three","four","five","six","seven", "eight","nine","ten"]
		@tensArray = Array["eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen",
		"eighteen","nineteen"]
		@twentyToNinety = Array["twenty","thirty","fourty","fifty","sixty","sevety","eighty","ninety"]

	end

	def doWork
		while @currentInt < 1001
			if    @currentInt < 11
			    self.Ones()
			elsif @currentInt < 20
				self.Tens()	
			elsif @currentInt < 100
				self.TwentyTo99()		
			elsif @currentInt < 1000
				self.Hundreds()
			else @currentInt == 1000
				@counter += 11								  				  						  				  	
			end
			@currentInt += 1
		end
		puts "for doWork #{@counter}"
	end

	def Ones()
		@counter += @onesArray[(@currentInt%10)-1].length
	end

	def Tens()
		@counter += @tensArray[(@currentInt%10)-1].length
	end

	def TwentyTo99()
			@counter += @twentyToNinety[(@currentInt/10)-2].length

			if @currentInt%10 != 0
				@counter += @onesArray[(@currentInt%10)-1].length
			end				
	end

	def pastOneHundred()
		temp = (@currentInt%100)

		@counter += @twentyToNinety[(temp/10)-2].length

		if (temp%10)-1 != 0
			@counter += @onesArray[(temp%10)-1].length
		end
	end

	def Hundreds()
		if (@currentInt%100 != 0) #add the length of 100 & if not equal to a scalar of 100 append "and"
			@counter += @onesArray[(@currentInt/100)-1].length + 7 + 3
		else
			@counter += @onesArray[(@currentInt/100)-1].length + 7
		end
		
		#account for the tens and ones spots
		case @currentInt%100
		 when (1..10)
		 	self.Ones()
		 when (11..19)
		 	self.Tens()	
		 when (20..99)
		 	self.pastOneHundred()			 	
		end  
	end

end


$obj = PE17.new
$obj.doWork